package main

import (
    "fmt"
    "net/http"
    "github.com/spf13/viper"
)

func hello(w http.ResponseWriter, req *http.Request) {

    fmt.Fprintf(w, "Hello World\n")
}

func headers(w http.ResponseWriter, req *http.Request) {

    for name, headers := range req.Header {
        for _, h := range headers {
            fmt.Fprintf(w, "%v: %v\n", name, h)
        }
    }
}

func main() {

    viper.SetConfigName("config")
    viper.AddConfigPath("/config/")
    viper.AddConfigPath(".")

    err := viper.ReadInConfig() // Find and read the config file
    if err != nil { // Handle errors reading the config file
        panic(fmt.Errorf("fatal error config file: %w", err))
    }

    entry_text := viper.GetString("entry-text")

    port := viper.GetInt("port")

    fmt.Print(entry_text, "\n")
	fmt.Print("Listening on port: ", port, "\n")

    http.HandleFunc("/hello", hello)
    http.HandleFunc("/headers", headers)

    http.ListenAndServe(":8090", nil)
}