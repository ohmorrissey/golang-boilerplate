# syntax=docker/dockerfile:1

## Build
FROM golang:1.19-buster AS build

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN go build -o /hello-world

## Deploy
FROM gcr.io/distroless/base-debian10

WORKDIR /

COPY --from=build /hello-world /hello-world
COPY config.yaml /config/

EXPOSE 8090

USER nonroot:nonroot

ENTRYPOINT ["/hello-world"]